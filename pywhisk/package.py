"""API operations related to packages.

Methods:
 * `get`: get a package
 * `create`: create a package
 * `update`: update an existing package, or create it if it does not exist
 * `delete`: delete a package
"""

import requests as r
from requests import JSONDecodeError

from .client import OpenWhiskException, MissingAuthenticationError
from .models import Package


def get(package_name, namespace, api_cfg):
    """Get a package.

    :param package_name: the name of the package
    :type package_name: str
    :param namespace: the namespace of the package
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration

    :returns: the package

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field
    """
    url = f'https://{api_cfg["host"]}/api/v1/namespaces/{namespace}/packages/{package_name}'

    try:
        auth = api_cfg['auth'][namespace]
    except KeyError:
        raise MissingAuthenticationError(f'missing authentication for namespace {namespace}',
                                         namespace=namespace) from None

    response = r.get(url, auth=auth, verify=api_cfg['cert'])

    try:
        response.raise_for_status()
    except r.HTTPError as err:
        try:
            # A server error from OpenWhisk.
            errbody = response.json()
            raise OpenWhiskException(
                f'failed getting package "/{namespace}/{package_name}": {errbody["error"]}') from err
        except JSONDecodeError:
            raise err from None

    package_dict = response.json()
    for key in ['name', 'namespace']:
        try:
            del package_dict[key]
        except KeyError:
            pass

    return Package.from_dict(package_dict)


def create(package, package_name, namespace, api_cfg):
    """Create a new package.

    :param package: the package to create
    :type package: Package
    :param package_name: the name of the package
    :type package_name: str
    :param namespace: the namespace of the package
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field
    """
    _update(package, package_name, namespace, api_cfg, overwrite=False)


def update(package, package_name, namespace, api_cfg):
    """Update an existing package, or create it if it does not exist.

    :param package: the package to update or create
    :type package: Package
    :param package_name: the name of the package
    :type package_name: str
    :param namespace: the namespace of the package
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field
    """

    _update(package, package_name, namespace, api_cfg, overwrite=True)


def _update(package, package_name, namespace, api_cfg, overwrite=False):
    """Backend function for `create` and `update`.

    :param package: the package to create
    :type package: Package
    :param package_name: the name of the package
    :type package_name: str
    :param namespace: the namespace of the package
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration
    :param overwrite: whether to overwrite an existing package with the same name
    :type overwrite: bool

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field
    """
    url = f'https://{api_cfg["host"]}/api/v1/namespaces/{namespace}/packages/{package_name}'

    try:
        auth = api_cfg['auth'][namespace]
    except KeyError:
        raise MissingAuthenticationError(f'missing authentication for namespace {namespace}',
                                         namespace=namespace) from None

    response = r.put(url, params={'overwrite': overwrite}, json=package.to_dict(), auth=auth, verify=api_cfg['cert'])

    try:
        response.raise_for_status()
    except r.HTTPError as err:
        try:
            # A server error from OpenWhisk.
            errbody = response.json()
            raise OpenWhiskException(
                f'failed updating package "/{namespace}/{package_name}": {errbody["error"]}') from err
        except JSONDecodeError:
            raise err from None


def delete(package_name, namespace, api_cfg, force=False):
    """Delete a package.

    :param package_name: the name of the package
    :type package_name: str
    :param namespace: the namespace of the package
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration
    :param force: whether to force delete the package if it contains entities
    :type force: bool

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field
    """
    url = f'https://{api_cfg["host"]}/api/v1/namespaces/{namespace}/packages/{package_name}'

    try:
        auth = api_cfg['auth'][namespace]
    except KeyError:
        raise MissingAuthenticationError(f'missing authentication for namespace {namespace}',
                                         namespace=namespace) from None

    response = r.delete(url, params={'force': force}, auth=auth, verify=api_cfg['cert'])

    try:
        response.raise_for_status()
    except r.HTTPError as err:
        try:
            # A server error from OpenWhisk.
            errbody = response.json()
            raise OpenWhiskException(
                f'failed deleting package "/{namespace}/{package_name}": {errbody["error"]}') from err
        except JSONDecodeError:
            raise err from None
