import random
import string
import uuid

from . import OpenWhiskAdminException
from .adminpod import AdminPod, AdminPodError


def create(user_name, admin_cfg):
    # code inspired by wskadmin, so it contains the same "rules" to handle input data
    if len(user_name) < 5:
        raise ValueError('user name must be at least 5 characters long')

    adminpod = AdminPod(admin_cfg['kubernetes'])

    # not handled: specified namespace
    namespace = user_name
    # not handled: specified uuid and key
    # same spec for UUID and key as from wskadmin
    uid = str(uuid.uuid4())
    key = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(64))

    try:
        adminpod.create_user(user_name, auth=f'{uid}:{key}')
    except AdminPodError as err:
        raise OpenWhiskAdminException(f'failed creating user with authentication {uid}:{key}: {str(err)}') from err

    return uid, key


def delete(user_name, admin_cfg):
    adminpod = AdminPod(admin_cfg['kubernetes'])

    try:
        adminpod.delete_user(user_name)
    except AdminPodError as err:
        raise OpenWhiskAdminException(str(err)) from err
