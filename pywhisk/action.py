"""API operations related to actions.

Methods:
 * `get`: get an action
 * `create`: create an action
 * `update`: update an existing action, or create it if it does not exist
 * `invoke`: invoke an action
 * `invoke_result`: invoke an action, blocking on its completion to only get its result
 * `delete`: delete an action
"""

import requests as r
from requests import JSONDecodeError

from .client import ActionError, BlockingActivationTimeout, OpenWhiskException, MissingAuthenticationError
from .models import Action, Activation


def get(action_name, namespace, code, api_cfg):
    """Get an action.

    :param action_name: the name of the action, including an optional package (ex: "pkg/action")
    :type action_name: str
    :param namespace: the namespace of the action
    :type namespace:str
    :param code: whether to fetch the action's source code
    :type code: bool
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration

    :returns: the action

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field

    The returned Action has its field `exec.code` set to None if `code` is False.
    """
    url = f'https://{api_cfg["host"]}/api/v1/namespaces/{namespace}/actions/{action_name}'

    try:
        auth = api_cfg['auth'][namespace]
    except KeyError:
        raise MissingAuthenticationError(f'missing authentication for namespace {namespace}',
                                         namespace=namespace) from None

    response = r.get(url, params={'code': code}, auth=auth, verify=api_cfg['cert'])

    try:
        response.raise_for_status()
    except r.HTTPError as err:
        try:
            # A server error from OpenWhisk.
            errbody = response.json()
            raise OpenWhiskException(f'failed getting action "/{namespace}/{action_name}": {errbody["error"]}') from err
        except JSONDecodeError:
            raise err from None

    action_dict = response.json()
    for key in ['name', 'namespace']:
        try:
            del action_dict[key]
        except KeyError:
            pass

    return Action.from_dict(action_dict)


def create(action, action_name, namespace, api_cfg):
    """Create a new action.

    :param action: the action to create
    :type action: Action
    :param action_name: the name of the action, including an optional package (ex: "pkg/action")
    :type action_name: str
    :param namespace: the namespace of the action
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field

    If the "code" of the Action is a Zip archive, it must be included in action.exec.code as a base64-encoded string.
    """
    _update(action, action_name, namespace, api_cfg, overwrite=False)


def update(action, action_name, namespace, api_cfg):
    """Update an existing action, or create it if it does not exist.

    :param action: the action to update or create
    :type action: Action
    :param action_name: the name of the action, including an optional package (ex: "pkg/action")
    :type action_name: str
    :param namespace: the namespace of the action
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field
    """

    _update(action, action_name, namespace, api_cfg, overwrite=True)


def _update(action, action_name, namespace, api_cfg, overwrite=False):
    """Backend function for `create` and `update`.

    :param action: the action to create
    :type action: Action
    :param action_name: the name of the action, including an optional package (ex: "pkg/action")
    :type action_name: str
    :param namespace: the namespace of the action
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration
    :param overwrite: whether to overwrite an existing action with the same name
    :type overwrite: bool

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field
    """
    url = f'https://{api_cfg["host"]}/api/v1/namespaces/{namespace}/actions/{action_name}'

    try:
        auth = api_cfg['auth'][namespace]
    except KeyError:
        raise MissingAuthenticationError(f'missing authentication for namespace {namespace}',
                                         namespace=namespace) from None

    response = r.put(url, params={'overwrite': overwrite}, json=action.to_dict(), auth=auth, verify=api_cfg['cert'])

    try:
        response.raise_for_status()
    except r.HTTPError as err:
        try:
            # A server error from OpenWhisk.
            errbody = response.json()
            raise OpenWhiskException(
                f'failed updating action "/{namespace}/{action_name}": {errbody["error"]}') from err
        except JSONDecodeError:
            raise err from None


def invoke(action_name, namespace, api_cfg, params=None, timeout=60000, blocking=False):
    """Invoke an action.

    :param action_name: the name of the action, including an optional package (ex: "pkg/action")
    :type action_name: str
    :param namespace: the namespace of the action
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration
    :param params: the parameters for the invocation (keys are parameter names and point to their values)
    :type params: dict
    :param timeout: the maximum time to wait for the invocation to finish
    :type timeout: int
    :param blocking: whether to block on the completion of the invocation
    :type blocking: bool

    :returns: the activation (an Activation, see below)

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field
    :raises ActionError: request succeeded but action failed; an Activation is available in the `activation` field

    If non blocking, the returned Activation has only one field filled in: the activation ID.
    """
    try:
        act_dict = _invoke(action_name, namespace, api_cfg, params=params, timeout=timeout, blocking=blocking,
                           result=False)
    except ActionError as err:
        try:
            del err.activation['namespace']
        except KeyError:
            pass
        err.activation = Activation.from_dict(err.activation)
        raise err
    except BlockingActivationTimeout as err:
        err.activation = Activation.from_dict(err.activation)
        raise err

    try:
        del act_dict['namespace']
    except KeyError:
        pass

    return Activation.from_dict(act_dict)


def invoke_result(action_name, namespace, api_cfg, params=None, timeout=60000):
    """Invoke an action and block on its completion waiting for its result.

    :param action_name: the name of the action, including an optional package (ex: "pkg/action")
    :type action_name: str
    :param namespace: the namespace of the action
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration
    :param params: the parameters for the invocation (mappings key-value)
    :type params: dict
    :param timeout: the maximum time to wait for the invocation to finish
    :type timeout: int

    :returns: the invocation result

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field

    Very similar to a blocking `invoke` call, except it only returns the result of the invocation (a Python structure
    resulting from decoding the result as JSON) instead of a full Activation record.
    """
    try:
        return _invoke(action_name, namespace, api_cfg, params=params, timeout=timeout, blocking=True, result=True)
    except BlockingActivationTimeout as err:
        err.activation = Activation.from_dict(err.activation)
        raise err


def _invoke(action_name, namespace, api_cfg, params=None, timeout=60000, blocking=False, result=False):
    """Backend function for `invoke` and `invoke_result`.

    :param action_name: the name of the action, including an optional package (ex: "pkg/action")
    :type action_name: str
    :param namespace: the namespace of the action
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration
    :param params: the parameters for the invocation (mappings key-value)
    :type params: dict
    :param timeout: the maximum time to wait for the invocation to finish
    :type timeout: int
    :param blocking: whether to block on the completion of the invocation
    :type blocking: bool
    :param result: whether to only return the result of the invocation when blocking, or a complete activation record
    :type result: bool

    :returns: the response to the query (a dict loaded from JSON, content varies, see below)

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field
    :raises ActionError: request succeeded but action failed; activation record (dict from JSON) in `activation` field
    :raises KeyError: missing authentication info for namespace in api_cfg.auth

    `result` has no effect if not blocking.

    The return value depends on `blocking` and `result`:

     * if non blocking, it contains an activation record;
     * if blocking and `result` is False, it contains a full activation record;
     * if blocking and `result` is True, it only contains the return value of the invocation
    """
    url = f'https://{api_cfg["host"]}/api/v1/namespaces/{namespace}/actions/{action_name}'

    try:
        auth = api_cfg['auth'][namespace]
    except KeyError:
        raise MissingAuthenticationError(f'missing authentication for namespace {namespace}',
                                         namespace=namespace) from None

    response = r.post(url, params={'blocking': blocking, 'result': result, 'timeout': timeout}, json=params,
                      auth=auth, verify=api_cfg['cert'])

    try:
        response.raise_for_status()
    except r.HTTPError as err:
        try:
            errbody = err.response.json()
        except JSONDecodeError:
            raise err from None

        if err.response.status_code == 502:
            raise ActionError(f'activation of "/{namespace}/{action_name}" produced an application error',
                              activation=errbody) from None

        raise OpenWhiskException(f'failed invoking action "/{namespace}/{action_name}": {errbody["error"]}') from err
    else:
        if response.status_code == 202:
            # 202 means that the returned activation record only contains the activationId. It might be expected if non
            # blocking, or unexpected if blocking, and this most probably indicates that the controller timed out
            # waiting for the action execution. THIS DOES NOT MEAN THE ACTION ITSELF TIMED OUT.
            if blocking:
                act_dict = response.json()

                raise BlockingActivationTimeout(f'blocking activation of "/{namespace}/{action_name}" timed out',
                                                activation=act_dict)

    return response.json()


def delete(action_name, namespace, api_cfg):
    """Delete an action.

    :param action_name: the name of the action, including an optional package (ex: "pkg/action")
    :type action_name: str
    :param namespace: the namespace of the action
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field
    """
    url = f'https://{api_cfg["host"]}/api/v1/namespaces/{namespace}/actions/{action_name}'

    try:
        auth = api_cfg['auth'][namespace]
    except KeyError:
        raise MissingAuthenticationError(f'missing authentication for namespace {namespace}',
                                         namespace=namespace) from None

    response = r.delete(url, auth=auth, verify=api_cfg['cert'])

    try:
        response.raise_for_status()
    except r.HTTPError as err:
        try:
            # A server error from OpenWhisk.
            errbody = response.json()
            raise OpenWhiskException(
                f'failed deleting action "/{namespace}/{action_name}": {errbody["error"]}') from err
        except JSONDecodeError:
            raise err from None
