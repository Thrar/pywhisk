"""API operations related to actions.

Methods:
 * `get`: get an activation record
 * `get_all`: get all activation records
 * `logs`: only get the logs of an activation
 * `result`: only get the result of an activation
"""

import requests as r
from requests import JSONDecodeError

from .client import OpenWhiskException, MissingAuthenticationError
from .models import Activation


def get(activation_id, namespace, api_cfg):
    """Get an activation record.

    :param activation_id: the ID of the activation
    :type activation_id: str
    :param namespace: the namespace of the activation
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration

    :returns: the activation record

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field
    """
    url = f'https://{api_cfg["host"]}/api/v1/namespaces/{namespace}/activations/{activation_id}'

    try:
        auth = api_cfg['auth'][namespace]
    except KeyError:
        raise MissingAuthenticationError(f'missing authentication for namespace {namespace}',
                                         namespace=namespace) from None

    response = r.get(url, auth=auth, verify=api_cfg['cert'])

    try:
        response.raise_for_status()
    except r.HTTPError as err:
        try:
            # A server error from OpenWhisk.
            errbody = response.json()
            raise OpenWhiskException(
                f'failed getting activation "/{namespace}/{activation_id}": {errbody["error"]}') from err
        except JSONDecodeError:
            raise err from None

    act_dict = response.json()
    try:
        del act_dict['namespace']
    except KeyError:
        pass

    return Activation.from_dict(act_dict)


def get_all(namespace, api_cfg, full=True, since=None, upto=None, limit=200):
    """Get all activation records.

    :param namespace: the namespace of the activation
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration
    :param full: whether to fetch full activation record
    :type full: bool
    :param since: fetch activation records older than this timestamp
    :type since: int

    :returns: the list of activation records

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field
    """
    url = f'https://{api_cfg["host"]}/api/v1/namespaces/{namespace}/activations'

    try:
        auth = api_cfg['auth'][namespace]
    except KeyError:
        raise MissingAuthenticationError(f'missing authentication for namespace {namespace}',
                                         namespace=namespace) from None

    params = dict(
            limit=limit,
            )
    if full:
        params['docs'] = 'true'
    if since is not None:
        params['since'] = since
    if upto is not None:
        params['upto'] = upto

    response = r.get(url, params=params, auth=auth, verify=api_cfg['cert'])

    try:
        response.raise_for_status()
    except r.HTTPError as err:
        try:
            # A server error from OpenWhisk.
            errbody = response.json()
            raise OpenWhiskException(
                f'failed getting activations: {errbody["error"]}') from err
        except JSONDecodeError:
            raise err from None

    acts_dicts = response.json()
    acts = []

    for act_dict in acts_dicts:
        try:
            del act_dict['namespace']
        except KeyError:
            pass

        acts.append(Activation.from_dict(act_dict))

    return acts


def logs(activation_id, namespace, api_cfg):
    """Get only the logs of an activation.

    :param activation_id: the ID of the activation
    :type activation_id: str
    :param namespace: the namespace of the activation
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration

    :returns: the logs of the activation (a list of str)

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field
    """
    url = f'https://{api_cfg["host"]}/api/v1/namespaces/{namespace}/activations/{activation_id}/logs'

    try:
        auth = api_cfg['auth'][namespace]
    except KeyError:
        raise MissingAuthenticationError(f'missing authentication for namespace {namespace}',
                                         namespace=namespace) from None

    response = r.get(url, auth=auth, verify=api_cfg['cert'])

    try:
        response.raise_for_status()
    except r.HTTPError as err:
        try:
            # A server error from OpenWhisk.
            errbody = response.json()
            raise OpenWhiskException(
                f'failed getting logs for activation "/{namespace}/{activation_id}": {errbody["error"]}') from err
        except JSONDecodeError:
            raise err from None

    return response.json()['logs']


def result(activation_id, namespace, api_cfg):
    """Get only the result of an activation.

    :param activation_id: the ID of the activation
    :type activation_id: str
    :param namespace: the namespace of the activation
    :type namespace:str
    :param api_cfg: the configuration to reach the API host
    :type api_cfg: APIConfiguration

    :returns: the result of the activation (a Python structure resulting from decoding the result as JSON)

    :raises requests.exceptions.HTTPError: request failed; HTTP response available in the `response` field
    """
    url = f'https://{api_cfg["host"]}/api/v1/namespaces/{namespace}/activations/{activation_id}/result'

    try:
        auth = api_cfg['auth'][namespace]
    except KeyError:
        raise MissingAuthenticationError(f'missing authentication for namespace {namespace}',
                                         namespace=namespace) from None

    response = r.get(url, auth=auth, verify=api_cfg['cert'])

    try:
        response.raise_for_status()
    except r.HTTPError as err:
        try:
            # A server error from OpenWhisk.
            errbody = response.json()
            raise OpenWhiskException(
                f'failed getting result for activation "/{namespace}/{activation_id}": {errbody["error"]}') from err
        except JSONDecodeError:
            raise err from None

    return response.json()['result']
