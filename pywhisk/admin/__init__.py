"""Operations related to the administration and the backends of OpenWhisk.

Content:
 * `docker`: operations related to the Docker containers used by OpenWhisk to run actions
 * `logs`: operations related to the logs of OpenWhisk's components
 * `user`: operations related to the users of OpenWhisk

Other:
 * `adminpod`: interact with the administration pod of OpenWhisk
"""

from kubernetes import config as k_config

k_config.load_kube_config()


class OpenWhiskAdminException(Exception):
    """General exception when an administrative operation with OpenWhisk failed."""
    pass
