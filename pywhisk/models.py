"""Models of data exchanged with OpenWhisk."""

from dataclasses import asdict as dc_asdict, dataclass, fields as dc_fields


@dataclass
class Model:
    """Base class for data model."""

    @classmethod
    def from_dict(cls, d):
        return cls(**d)

    def to_dict(self):
        """Convert the data to a dict with only the fields that are not None.

        This is used before sending the data to the API server, to prune the missing values.
        """
        return dc_asdict(self, dict_factory=lambda l: dict([(k, v) for k, v in l if v is not None]))

    def __str__(self):
        fields = [(fn.name, getattr(self, fn.name)) for fn in dc_fields(self)]
        return self.__class__.__qualname__ + '(' + ', '.join(
            f'{name}={val}' for name, val in fields if val is not None) + ')'


@dataclass
class ActionExec(Model):
    """Action definition (code or container specification)."""
    kind: str  # XXX technically an enum
    binary: bool = None
    code: str = None
    image: str = None
    main: str = None
    components: [str] = None

    def __str__(self):
        # override to skip printing code
        fields = [(fn.name, getattr(self, fn.name)) for fn in dc_fields(self) if fn.name != 'code']
        if self.code is not None:
            fields.append(('code', '[...]'))
        return self.__class__.__qualname__ + '(' + ', '.join(
            f'{name}={val}' for name, val in fields if val is not None) + ')'


@dataclass
class ActionLimits(Model):
    """Limits on an action."""
    timeout: int = None
    memory: int = None
    logs: int = None
    concurrency: int = None


@dataclass
class Action(Model):
    """Action."""
    exec: ActionExec
    version: str = None
    limits: ActionLimits = None
    annotations: [dict] = None
    parameters: [dict] = None
    publish: bool = None
    updated: int = None

    @classmethod
    def from_dict(cls, d):
        d['exec'] = ActionExec.from_dict(d['exec'])
        try:
            d['limits'] = ActionLimits.from_dict(d['limits'])
        except KeyError:
            pass
        return super().from_dict(d)


@dataclass
class ActivationResult(Model):
    """Result of an activation."""
    status: str
    result: dict
    success: bool
    size: int = None


@dataclass
class Activation(Model):
    """Activation."""
    activationId: str
    name: str = None
    version: str = None
    publish: str = None
    annotations: [dict] = None
    subject: str = None
    start: int = None
    end: int = None
    duration: int = None
    response: ActivationResult = None
    logs: [str] = None
    cause: str = None
    statusCode: int = None

    @classmethod
    def from_dict(cls, d):
        try:
            d['response'] = ActivationResult.from_dict(d['response'])
        except KeyError:
            pass
        return super().from_dict(d)


@dataclass
class Package(Model):
    """Package."""
    version: str = None
    publish: bool = None
    annotations: [dict] = None
    parameters: [dict] = None
    binding: [dict] = None
    actions: [dict] = None
    feed: [dict] = None
    updated: int = None
