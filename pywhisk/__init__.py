"""PyWhisk: Python interface to Apache OpenWhisk's REST API

Content:
 * `client`: facilities for the client using PyWhisk
 * `models`: models of data exchanged with OpenWhisk
 * `action`, `activation`, `packages`: modules implementing scopes of OpenWhisk operations, mimicking the `wsk` CLI
 * `admin`: OpenWhisk administration operations, mimicking `wskadmin`
"""
