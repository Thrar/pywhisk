"""Operations related to the logs of OpenWhisk's components.

Note that the methods in this module interact directly with Kubernetes to fetch the pods' logs;
i.e., it does not use the commands of the administration pod `AdminPod`.

Methods:
 * `get_transaction_logs`: get the logs of a component relative to a transaction or to the transaction of an activation
 * `get_logs`: get all the logs of a component
 * `get_transaction_id`: get the internal ID of the transaction that served an activation
"""

import re
from enum import auto, Enum

from kubernetes import client as k_client
from kubernetes.client import ApiException as kApiException
from urllib3.exceptions import MaxRetryError

from . import OpenWhiskAdminException


class Components(Enum):
    """OpenWhisk's components that have logs to fetch for."""
    Invoker = auto()
    Controller = auto()


def get_transaction_logs(component, admin_cfg, instance=None, transaction_id=None, activation_id=None,
                         controller_instance=None):
    """Get logs from `components` relative to a transaction, or to the transaction of an activation.

    :param component: the components from which to fetch the logs
    :type component: Components
    :param admin_cfg: the configuration to administrate the OpenWhisk installation
    :type admin_cfg: AdministrationConfiguration
    :param instance: the component instance number from which to fetch the log, if known
    :type instance: int | None
    :param transaction_id: the ID of the transaction of which to fetch the logs
    :type transaction_id: str
    :param activation_id: the ID of the activation of which to fetch the transaction logs
    :type activation_id: str
    :param controller_instance: the controller instance number from which to get the transaction ID of the activation,
        if known
    :type controller_instance: int | None

    :return: the log lines of the transaction

    `transaction_id` and `activation_id` are mutually exclusive. If only `activation_id` is given, its transaction ID is
    first obtained, otherwise `transaction_id` is used.
    """
    if transaction_id and activation_id or not transaction_id and not activation_id:
        raise ValueError('must specify exactly one of transaction_id or activation_id')

    if activation_id:
        try:
            transaction_id = get_transaction_id(activation_id, admin_cfg, controller_instance=controller_instance)
        except OpenWhiskAdminException as err:
            raise OpenWhiskAdminException(f'failed getting transaction ID for activation {activation_id}') from err

    try:
        return [line for line in get_logs(component, admin_cfg, instance=instance) if
                f'[#tid_{transaction_id}]' in line]
    except (kApiException, MaxRetryError) as err:
        raise OpenWhiskAdminException(f'failed getting logs from {component} instance {instance}') from err


def get_logs(component, admin_cfg, instance=None, since_seconds=None):
    """Get all the logs of the component.

    :returns: the log lines of the component (list of str)
    """
    kcli = k_client.CoreV1Api()

    if instance is not None:
        invoker_logs = kcli.read_namespaced_pod_log(
            name=f'{admin_cfg["kubernetes"]["helmrelease"]}-{component.name.lower()}-{instance}',
            namespace=admin_cfg["kubernetes"]["namespace"],
            since_seconds=since_seconds)
    else:
        pods = kcli.list_namespaced_pod(namespace=admin_cfg['kubernetes']['namespace'], label_selector=f'name={admin_cfg["kubernetes"]["helmrelease"]}-{component.name.lower()}').items
        invoker_logs = '\n'.join(kcli.read_namespaced_pod_log(
            name=pod.metadata.name,
            namespace=admin_cfg["kubernetes"]["namespace"],
            since_seconds=since_seconds) for pod in pods)

    return invoker_logs.split('\n')


def get_transaction_id(activation_id, admin_cfg, controller_instance=None):
    """Get the transaction ID that served the given activation."""
    # assuming activation_id really is the activation ID (no brackets or other special characters)
    transaction_id_re = re.compile(
        r'\[#tid_([0-9a-zA-Z]+)\].*' + activation_id + r'.*\[marker:controller_loadbalancer_start:.*\]')
    try:
        for line in get_logs(Components.Controller, admin_cfg, instance=controller_instance):
            re_match = transaction_id_re.search(line)
            if re_match:
                return re_match[1]
    except kApiException as err:
        raise OpenWhiskAdminException(f'failed getting logs from controller instance {controller_instance}') from err

    raise ValueError('could not find the transaction ID')
