"""Operations related to the Docker containers used by OpenWhisk to run actions.

Methods:
 * `get_docker_id`: get the ID of the Docker container that ran a given activation
"""

import re

from . import OpenWhiskAdminException
from .logs import Components, get_transaction_logs


def get_docker_id(activation_id, admin_cfg):
    """Get the ID of the Docker container that ran the given activation.

    :param activation_id: the activation ID
    :type activation_id: str
    :param admin_cfg: the configuration for administration operations
    :type admin_cfg: AdministrationConfiguration

    :returns: the ID of the Docker container

    :raises ValueError: could not find the container ID of the activation

    This method fetches the logs of OpenWhisk's invoker and filter them for the lines about the given activation ID,
    looking for a certain line that gives the ID of the container used by the invocation.
    """
    try:
        logs = get_transaction_logs(Components.Invoker, admin_cfg, activation_id=activation_id)
    except OpenWhiskAdminException as err:
        raise OpenWhiskAdminException(f'failed getting transaction logs from the invoker') from err

    # I look for the log line saying that arguments are sent to this container, just to be sure I get the ID of the
    # container that actually ran the function
    cont_id_re = re.compile(r'sending arguments to .* at ContainerId\(([0-9a-zA-Z-]+)\)')
    for line in logs:
        re_match = cont_id_re.search(line)
        if re_match:
            return re_match[1]

    raise ValueError('could not find the container ID')
