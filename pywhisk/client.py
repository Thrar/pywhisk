"""Module with facilities for the client.

Classes:

 * `APIConfiguration`: the dict of configuration to reach OpenWhisk's API
 * `AdministrationConfiguration`: the dict of configuration to reach OpenWhisk's administration interface

Methods:

 * `read_api_cfg`: get an APIConfiguration by reading a "wskprops" file
 * `read_manifest`: read functions and packages from an OpenWhisk manifest file

Exceptions:

 * `OpenWhiskException`
 * `ActionError`
 * `BlockingActivationTimeout`
"""

import os
from base64 import b64encode
from typing import TypedDict, Mapping, Tuple

import yaml
from kubernetes import config as k_config

from .models import Action, ActionExec, ActionLimits


class APIConfiguration(TypedDict):
    host: str
    cert: bool | str
    auth: Mapping[str, Tuple[str, str]]


class KubernetesConfiguration(TypedDict):
    helmrelease: str
    namespace: str


class AdministrationConfiguration(TypedDict):
    kubernetes: KubernetesConfiguration


def read_api_cfg(path='~/.wskprops', cert=True) -> APIConfiguration:
    """Read the configuration file to access the management REST API.

    :param path: the path to the properties file
    :type path: str
    :param cert: str: the path to the SSL certificate of the API; bool: whether to enable certificate checking
    :type cert: str, bool

    :returns: the API configuration (a namedtuple APIConfiguration)

    If `cert` is a str, it should point to the certificate used by the API, which is most useful when using the
    self-signed certificate that cannot be verified. If `cert` is a bool, True means that the certificate of the API
    should be verified normally (i.e. from the CAs installed on the host), and False disables the verification.
    If `cert` as a str does not point to a valid certificate, it is interpreted as True.

    The returned value is what you should pass to every API call as the `api_cfg` argument, with the following fields:

     * host: str, the URL to the API host (everything before "/api/v1/...", without protocol)
     * auth: dict mapping namespaces to tuples of str (both halves of the authorization key), see below
     * cert: path to the certificate of the API, or whether to enable verification of the certificate

    The field `auth` gives the authorization key for a given namespace. This is meant to be used like a keyring of some
    sort, but this method can only fill it with the authorization key for the default namespace ("/_"). You have to fill
    it manually to add more namespaces.
    """
    k_config.load_kube_config()

    path = os.path.expanduser(os.path.expandvars(path))

    with open(path, 'r') as propsfile:
        props = dict([tuple(line.rstrip().split('=', maxsplit=1)) for line in propsfile])

    if isinstance(cert, str):
        cert = os.path.expanduser(os.path.expandvars(cert))
        if not os.path.isfile(cert):
            # This value is used as the parameter `verify` of HTTP method calls of the Requests library, so if no valid
            # certificate has been given, reset it.
            cert = True

    return APIConfiguration(host=props['APIHOST'], auth={'_': tuple(props['AUTH'].split(':', maxsplit=1))},
                            cert=cert)


def read_manifest(manifest_path):
    """Read functions from the Manifest.

    :param manifest_path: path to the Manifest
    :type manifest_path: str

    :return: dictionary mapping package names to dicts, mapping functions names to Action
    """
    with open(manifest_path, 'r') as manifest_file:
        manifest = yaml.full_load(manifest_file)

    return {
        pkg_name: {
            action_name: _compose_action(action_dict, os.path.dirname(manifest_path))
            for action_name, action_dict in pkg_dict['actions'].items()
        }
        for pkg_name, pkg_dict in manifest['packages'].items()
    }


LIMIT_NAMES = {
    'memorySize':            'memory',
    'timeout':               'timeout',
    'logSize':               'logs',
    'concurrentActivations': 'concurrency',
}

# "Codes" of functions that end with these extensions should be treated as "binary", and encoded in Base64.
BINARY_EXTS = ['.jar', '.zip', '.balx']


def _compose_action(d, function_basepath):
    """Create an Action from the dictionary read from the Manifest.

    :param d: dictionary representing an action as loaded from the Manifest file
    :type d: dict
    :param function_basepath: base path of functions' codes
    :type function_basepath: str

    :return: the Action
    """

    _, code_file_ext = os.path.splitext(d['function'])
    with open(os.path.join(function_basepath, d['function']), 'rb') as code_file:
        if code_file_ext in BINARY_EXTS:
            code = b64encode(code_file.read()).decode()
        else:
            code = code_file.read().decode()
        try:
            ex = ActionExec(kind=d['runtime'], code=code, main=d.get('main', None))
        except KeyError:
            ex = ActionExec(kind='blackbox', image=d['docker'], code=code)

    if 'limits' in d:
        limits = ActionLimits.from_dict(
            {LIMIT_NAMES[limit_name]: limit_value for limit_name, limit_value in d['limits'].items()})
    else:
        limits = None

    return Action(exec=ex,
                  limits=limits,
                  annotations=[{'key': anno_name, 'value': anno_value} for anno_name, anno_value in
                               d['annotations'].items()])


class OpenWhiskException(Exception):
    """General exception when an operation with OpenWhisk failed."""
    pass


class MissingAuthenticationError(KeyError):
    """The authentication token for a namespace is missing."""

    def __init__(self, msg, namespace):
        super().__init__(msg)

        self.namespace = namespace

    def __str__(self):
        return self.args[0]


class ActionError(Exception):
    """An action activation produced an application error ("developer error").

    Field `activation` may contain the Activation, unless the action was invoked in "result-only" mode.
    """

    def __init__(self, msg, activation=None):
        super().__init__(msg)

        self.activation = activation


class BlockingActivationTimeout(Exception):
    """A blocking activation timed out waiting for the action to finish.

    This does not indicate that the action itself timed out.

    Field `activation` contains the Activation (with only its field `activationId` set)."""

    def __init__(self, msg, activation):
        super().__init__(msg)

        self.activation = activation
