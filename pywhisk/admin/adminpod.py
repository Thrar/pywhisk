"""The administration pod of OpenWhisk, i.e., the pod where the command wskadmin runs.

When deployed in a Kubernetes cluster, OpenWhisk creates an administration pod "wskadmin", where administration commands
can be run interactively.
`AdminPod` can be used to utilize it.

Classes:

 * `AdminPod`: a connection to OpenWhisk's administration pod

Exceptions:

 * `AdminPodError`
"""

from __future__ import annotations

from typing import Sequence, Optional, Iterable

from kubernetes import client as k_client
from kubernetes.client import ApiException
from kubernetes.stream import stream as kstream

from ..client import KubernetesConfiguration


class AdminPod:
    """A connection to the administration pod of OpenWhisk.

    Not all methods are implemented.
    They all use `_exec` underneath.

    See also Kubernetes's Python client library: https://github.com/kubernetes-client/python.
    """
    def __init__(self,
                 kubernetes_cfg: KubernetesConfiguration,
                 podname: Optional[str] = 'wskadmin',
                 podcommand: Optional[Sequence[str]] = None,
                 timeout: Optional[int] = 10):
        if podcommand is None:
            podcommand = ['wskadmin']

        self.kcli = k_client.CoreV1Api()

        self.cfg = kubernetes_cfg

        self.name = f'{self.cfg["helmrelease"]}-{podname}'
        self.command = podcommand
        self.timeout = timeout

    def _exec(self, cmd: Sequence[str]) -> kstream:
        try:
            exec_stream = kstream(self.kcli.connect_get_namespaced_pod_exec,
                                  self.name,
                                  self.cfg['namespace'],
                                  command=cmd,
                                  stderr=True, stdin=False,
                                  stdout=True, tty=False,
                                  _preload_content=False)
        except ApiException as err:
            raise AdminPodError(f'failed executing command `{" ".join(cmd)}` in pod {self.name} of namespace '
                                f'{self.cfg["namespace"]}') from err

        exec_stream.run_forever()
        if exec_stream.is_open():
            raise AdminPodError(f'failed executing command `{" ".join(cmd)}`: timed out')

        if exec_stream.returncode != 0:
            raise AdminPodError(
                f'command `{" ".join(cmd)}` exited with code {exec_stream.returncode}:\n{exec_stream.read_all()}')

        return exec_stream

    def get_user(self, user_name: str, wskadmin_subcommand: Optional[Iterable[str]] = None) -> list[str]:
        if wskadmin_subcommand is None:
            wskadmin_subcommand = ['user', 'get']

        cmd = self.command + wskadmin_subcommand + [user_name]

        exec_stream = self._exec(cmd)

        return exec_stream.readline_stdout().split(':')

    def create_user(self, user_name: str, auth: str, wskadmin_subcommand: Optional[Iterable[str]] = None):
        if wskadmin_subcommand is None:
            wskadmin_subcommand = ['user', 'create']

        cmd = self.command + wskadmin_subcommand + ['-s', '-u', auth, user_name]

        self._exec(cmd)

    def delete_user(self, user_name: str, wskadmin_subcommand: Optional[Iterable[str]] = None):
        if wskadmin_subcommand is None:
            wskadmin_subcommand = ['user', 'delete']

        cmd = self.command + wskadmin_subcommand + [user_name]

        self._exec(cmd)


class AdminPodError(Exception):
    """An error occurred while interacting with the administration pod."""
    pass
