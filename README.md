# PyWhisk: Python library to Apache OpenWhisk's REST API

A (yet incomplete) Python interface to OpenWhisk's REST API to manage actions, activations, as well as administration tasks.

The implemented API is taken from the documentation for [OpenWhisk REST API](http://petstore.swagger.io/?url=https://raw.githubusercontent.com/apache/incubator-openwhisk/master/core/controller/src/main/resources/apiv1swagger.json).

As for the administrative API, PyWhisk is only compatible with a Kubernetes deployment of OpenWhisk (see [pywhisk/admin/adminpod.py](pywhisk/admin/adminpod.py) for more information).

Refer to the modules' documentation for complete usage documentation.

## Installation

Install with pip (or pipenv):

```shell
pip install git+https://gitlab.com/Thrar/pywhisk.git#egg=pywhisk
```

## Example: action invocation

```python
from pywhisk import action
from pywhisk.client import OpenWhiskException, read_api_cfg

# Get OpenWhisk configuration to reach the API.
wsk_cfg = read_api_cfg(cert=False)
# Invoke an action, blocking on its completion.
try:
    activation = action.invoke(f'samples/greeting', f'whisk.system', wsk_cfg, {'name': 'World', 'place': 'Universe'},
                               blocking=True)
except OpenWhiskException as err:
    print(f'activation failed\n{err}')
else:
    print(activation.response.result)
```
